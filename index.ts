let myName: string = "Tum Kittichai";
let age: number = 24;
let alive: boolean = true;
let personal: any = `Name: ${myName}, Age: ${age}, Alive: ${alive}`;

console.log(myName); // Tum Kittichai
console.log(age); // 24
console.log(alive); // true
console.log(personal); // 